import { useState, useEffect } from "react";

import Loading from "../../../components/Loading";
import API from "../../../utils/API";
import { useHistory, useParams } from "react-router-dom";
import { Button, Grid, Paper, TextField } from "@material-ui/core";

const EditClient = () => {
  const { id } = useParams();
  const [LoadingOpen, setLoadingOpen] = useState(false);
  const [data, setData] = useState({
    name: "",
    email: "",
    phone: "",
  });

  const history = useHistory();
  useEffect(() => {
    const fecthData = async (e) => {
      const res = await API(
        "GET",
        `${process.env.REACT_APP_API_BASE_URL}/clients/${id}`
      );
      if (res.code === 200) {
        setData({
          name: res.data.name,
          email: res.data.email,
          phone: res.data.phone,
        });
      }
    };
    fecthData();
    return () => {
      setData([]);
    };
  }, [id]);

  const handleFormChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };
  const onSubmitHandler = async (e) => {
    e.preventDefault();

    const name = e.target.name.value;
    const email = e.target.email.value;
    const phone = e.target.phone.value;

    if (!name && !email && !phone) {
      alert("il faut remplir les champs");
      return false;
    }
    try {
      setLoadingOpen(true);

      const res = await API(
        "PUT",
        `${process.env.REACT_APP_API_BASE_URL}/clients/${id}`,
        { name, email, phone }
      );

      if (res.code === 200) {
        alert("Client modifié avec sucées");
        history.push("/clients");
      } else {
        alert(
          "Une erreur est survenue, Veuillez réessayer, si l'erreur persiste contacter l'administrateur du site."
        );
      }
      setLoadingOpen(false);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <Loading open={LoadingOpen} />
      <Grid justify="center" alignItems="center" container spacing={3}>
        <Grid item xs={6}>
          <h2>Modifier Type </h2>
          <Paper
            style={{
              padding: "20px",
            }}
          >
            <form onSubmit={onSubmitHandler}>
              <div align="center" style={{ marginBottom: "20px" }}>
                <TextField
                  id="standard-basic"
                  label="Nom"
                  name="name"
                  value={data.name ?? ""}
                  onChange={handleFormChange}
                  fullWidth
                />
              </div>
              <div align="center" style={{ marginBottom: "20px" }}>
                <TextField
                  id="standard-basic"
                  label="Email"
                  name="email"
                  value={data.email ?? ""}
                  onChange={handleFormChange}
                  fullWidth
                />
              </div>
              <div align="center" style={{ marginBottom: "20px" }}>
                <TextField
                  id="standard-basic"
                  label="Phone"
                  name="phone"
                  value={data.phone ?? ""}
                  onChange={handleFormChange}
                  fullWidth
                />
              </div>

              <div align="center">
                <Button variant="contained" color="primary" type="submit">
                  Mettre à jour
                </Button>
              </div>
            </form>
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};

export default EditClient;
