import { useState, useEffect } from "react";
import Loading from "../../../components/Loading";
import API from "../../../utils/API";
import { useHistory } from "react-router-dom";
import {
  Button,
  Grid,
  MenuItem,
  Paper,
  Select,
  TextField,
  InputLabel,
  FormControl,
} from "@material-ui/core";

const AddProject = () => {
  const [LoadingOpen, setLoadingOpen] = useState(false);
  const [client, setClient] = useState([]);

  const history = useHistory();
  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await API(
          "GET",
          `${process.env.REACT_APP_API_BASE_URL}/clients`
        );
        setClient(res.data);
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
  }, []);
  const onSubmitHandler = async (e) => {
    e.preventDefault();
    const name = e.target.name.value;
    const description = e.target.description.value;
    const client = e.target.client.value;

    if (!name && !description) {
      alert("il faut remplir le champ");
      return false;
    }
    try {
      setLoadingOpen(true);
      const res = await API(
        "POST",
        `${process.env.REACT_APP_API_BASE_URL}/projects`,
        { name, description, client }
      );
      if (res.code === 200) {
        alert("Projet ajouté avec succés");
        history.push("/projects");
      } else {
        alert(
          "Une erreur est survenue, Veuillez réessayer, si l'erreur persiste contacter l'administrateur du site."
        );
      }
    } catch (error) {
      setLoadingOpen(false);
      console.log(error);
    }
  };

  return (
    <>
      <Loading open={LoadingOpen} />
      <Grid justify="center" alignItems="center" container spacing={3}>
        <Grid item xs={6}>
          <h2>Ajouter Projet </h2>
          <Paper
            style={{
              padding: "20px",
            }}
          >
            <form onSubmit={onSubmitHandler}>
              <div align="center" style={{ marginBottom: "20px" }}>
                <TextField
                  id="standard-basic"
                  label="Nom"
                  name="name"
                  fullWidth
                />
              </div>
              <div align="center" style={{ marginBottom: "20px" }}>
                <TextField
                  id="standard-basic"
                  label="Déscription"
                  name="description"
                  fullWidth
                />
              </div>
              <FormControl fullWidth style={{ marginBottom: "20px" }}>
                <InputLabel id="location_label">Client</InputLabel>
                <Select
                  labelId="client_label"
                  id="clientId"
                  name="client"
                  fullWidth
                >
                  {client.map((item) => (
                    <MenuItem value={item.id}>{item.name}</MenuItem>
                  ))}
                </Select>
              </FormControl>
              <div align="center">
                <Button variant="contained" color="primary" type="submit">
                  Ajouter
                </Button>
              </div>
            </form>
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};

export default AddProject;
