import { useState, useEffect } from "react";

import Loading from "../../../components/Loading";
import API from "../../../utils/API";
import { useHistory, useParams } from "react-router-dom";
import {
  Button,
  Grid,
  MenuItem,
  Paper,
  Select,
  TextField,
  InputLabel,
  FormControl,
} from "@material-ui/core";

const EditProject = () => {
  const { id } = useParams();
  const [LoadingOpen, setLoadingOpen] = useState(false);
  const [data, setData] = useState({
    name: "",
    description: "",
  });

  const [client, setClient] = useState([]);

  const onClientChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const history = useHistory();
  useEffect(() => {
    const initData = async (e) => {
      try {
        const res = await API(
          "GET",
          `${process.env.REACT_APP_API_BASE_URL}/clients`
        );
        setClient(res.data);
      } catch (error) {
        console.log(error);
      }
    };
    initData();

    const fecthData = async (e) => {
      const res = await API(
        "GET",
        `${process.env.REACT_APP_API_BASE_URL}/projects/${id}`
      );
      if (res.code === 200) {
        setData({
          name: res.data.name,
          description: res.data.description,
          client: res.data.client ? res.data.client.id : "",
        });
      }
    };
    fecthData();
    return () => {
      setData([]);
    };
  }, [id]);

  const handleFormChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const onSubmitHandler = async (e) => {
    e.preventDefault();

    const name = e.target.name.value;
    const description = e.target.description.value;
    const client = e.target.client.value;

    if (!name && !description) {
      alert("Il faut remplir le champ");
      return false;
    }

    try {
      setLoadingOpen(true);
      // save data api here

      const res = await API(
        "PUT",
        `${process.env.REACT_APP_API_BASE_URL}/projects/${id}`,
        { name, description, client }
      );

      if (res.code === 200) {
        alert("Projet modifié avec succés");
        history.push("/projects");
      } else {
        alert(
          "Une erreur est survenue, Veuillez réessayer, si l'erreur persiste contacter l'administrateur du site."
        );
      }
      setLoadingOpen(false);
    } catch (error) {
      setLoadingOpen(false);
      console.log(error);
    }
  };

  return (
    <>
      <Loading open={LoadingOpen} />
      <Grid justify="center" alignItems="center" container spacing={3}>
        <Grid item xs={6}>
          <h2>Modifier Projet </h2>
          <Paper
            style={{
              padding: "20px",
            }}
          >
            <form onSubmit={onSubmitHandler}>
              <div align="center" style={{ marginBottom: "20px" }}>
                <TextField
                  id="standard-basic"
                  label="Nom"
                  name="name"
                  value={data.name ?? ""}
                  onChange={handleFormChange}
                  fullWidth
                />
              </div>
              <div align="center" style={{ marginBottom: "20px" }}>
                <TextField
                  id="standard-basic"
                  label="Description"
                  name="description"
                  value={data.description ?? ""}
                  onChange={handleFormChange}
                  fullWidth
                />
              </div>
              <FormControl fullWidth style={{ marginBottom: "20px" }}>
                <InputLabel id="location_label">Client</InputLabel>
                <Select
                  labelId="client_label"
                  id="clientId"
                  value={data.client ? data.client : ""}
                  name="client"
                  fullWidth
                  onChange={(e) => {
                    onClientChange(e, "client");
                  }}
                >
                  {client.map((item) => (
                    <MenuItem value={item.id}>{item.name}</MenuItem>
                  ))}
                </Select>
              </FormControl>
              <div align="center">
                <Button variant="contained" color="primary" type="submit">
                  Mettre à jour
                </Button>
              </div>
            </form>
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};

export default EditProject;
