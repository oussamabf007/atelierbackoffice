import { useState, useEffect } from "react";

import Loading from "../../../components/Loading";
import API from "../../../utils/API";
import { useHistory, useParams } from "react-router-dom";
import { Button, Grid, Paper, TextField } from "@material-ui/core";

const EditWorker = () => {
  const { id } = useParams();
  const [LoadingOpen, setLoadingOpen] = useState(false);
  const [data, setData] = useState({
    name: "",
    cost: "",
    post: "",
    phone: "",
  });
  const history = useHistory();
  useEffect(() => {
    const fetchData = async (e) => {
      const res = await API(
        "GET",
        `${process.env.REACT_APP_API_BASE_URL}/workers/${id}`
      );
      if (res.code === 200) {
        setData({
          name: res.data.name,
          cost: res.data.cost,
          post: res.data.post,
          phone: res.data.phone,
        });
      }
    };
    fetchData();
    return () => {
      setData([]);
    };
  }, [id]);

  const handleFormChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const onSubmitHandler = async (e) => {
    e.preventDefault();

    const name = e.target.name.value;
    const cost = e.target.cost.value;
    const post = e.target.post.value;
    const phone = e.target.phone.value;
    if (!name && !cost) {
      alert("il faut remplir les champs");
      return false;
    }

    try {
      setLoadingOpen(true);

      const res = await API(
        "PUT",
        `${process.env.REACT_APP_API_BASE_URL}/workers/${id}`,
        { name, cost, post, phone }
      );
      if (res.code === 200) {
        alert("Worker modifié avec sucées");
        history.push("/workers");
      } else {
        alert(
          "Une erreur est survenue, Veuillez réessayer, si l'erreur persiste contacter l'administrateur du site."
        );
      }
      setLoadingOpen(false);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <Loading open={LoadingOpen} />
      <Grid justify="center" alignItems="center" container spacing={3}>
        <Grid item xs={6}>
          <h2>Modifier Worker </h2>
          <Paper
            style={{
              padding: "20px",
            }}
          >
            <form onSubmit={onSubmitHandler}>
              <div align="center" style={{ marginBottom: "20px" }}>
                <TextField
                  id="standard-basic"
                  label="Nom"
                  name="name"
                  value={data.name ?? ""}
                  onChange={handleFormChange}
                  fullWidth
                />
              </div>
              <div align="center" style={{ marginBottom: "20px" }}>
                <TextField
                  id="standard-basic"
                  label="Cout"
                  name="cost"
                  type="number"
                  value={data.cost ?? ""}
                  onChange={handleFormChange}
                  fullWidth
                />
              </div>
              <div align="center" style={{ marginBottom: "20px" }}>
                <TextField
                  id="standard-basic"
                  label="Post"
                  name="post"
                  value={data.post ?? ""}
                  onChange={handleFormChange}
                  fullWidth
                />
              </div>
              <div align="center" style={{ marginBottom: "20px" }}>
                <TextField
                  id="standard-basic"
                  label="Phone"
                  name="phone"
                  value={data.phone ?? ""}
                  onChange={handleFormChange}
                  fullWidth
                />
              </div>
              <div align="center">
                <Button variant="contained" color="primary" type="submit">
                  Mettre à jour
                </Button>
              </div>
            </form>
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};

export default EditWorker;
