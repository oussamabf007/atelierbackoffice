import { Switch, Redirect } from "react-router-dom";

import DefaultLayout from "../layouts/DefaultLayout";
import NotFound from "../layouts/404";

import { Route } from "react-router-dom";

import Dashboard from "../views/Dashboard";
import Login from "../views/Login";

import Settings from "../views/Settings";
import RouteWithLayout from "./RouteWithLayout";
import Project from "../views/Project";
import AddProject from "../views/Project/AddProject";
import EditProject from "../views/Project/EditProject";
import Task from "../views/Task";
import AddTask from "../views/Task/AddTask";
import EditTask from "../views/Task/EditTask";
import Worker from "../views/Worker";
import AddWorker from "../views/Worker/AddWorker";
import EditWorker from "../views/Worker/EditWorker";
import Client from "../views/Client";
import AddClient from "../views/Client/AddClient";
import EditClient from "../views/Client/EditClient";
import Type from "../views/Type";
import AddType from "../views/Type/AddType";
import EditType from "../views/Type/EditType";

const Routes = () => {
  return (
    <Switch>
      <RouteWithLayout
        component={Dashboard}
        exact
        layout={DefaultLayout}
        path="/"
      />
      <RouteWithLayout
        component={Project}
        exact
        layout={DefaultLayout}
        path="/projects"
      />
      <RouteWithLayout
        component={AddProject}
        exact
        layout={DefaultLayout}
        path="/projects/new"
      />
      <RouteWithLayout
        component={EditProject}
        exact
        layout={DefaultLayout}
        path="/projects/:id/update"
      />
      <RouteWithLayout
        component={Task}
        exact
        layout={DefaultLayout}
        path="/tasks"
      />
      <RouteWithLayout
        component={AddTask}
        exact
        layout={DefaultLayout}
        path="/tasks/new"
      />
      <RouteWithLayout
        component={EditTask}
        exact
        layout={DefaultLayout}
        path="/tasks/:id/update"
      />
      <RouteWithLayout
        component={Worker}
        exact
        layout={DefaultLayout}
        path="/workers"
      />
      <RouteWithLayout
        component={AddWorker}
        exact
        layout={DefaultLayout}
        path="/workers/new"
      />
      <RouteWithLayout
        component={EditWorker}
        exact
        layout={DefaultLayout}
        path="/workers/:id/update"
      />
      <RouteWithLayout
        component={Client}
        exact
        layout={DefaultLayout}
        path="/clients"
      />
      <RouteWithLayout
        component={AddClient}
        exact
        layout={DefaultLayout}
        path="/clients/new"
      />
      <RouteWithLayout
        component={EditClient}
        exact
        layout={DefaultLayout}
        path="/clients/:id/update"
      />
      <RouteWithLayout
        component={Type}
        exact
        layout={DefaultLayout}
        path="/types"
      />
      <RouteWithLayout
        component={AddType}
        exact
        layout={DefaultLayout}
        path="/types/new"
      />
      <RouteWithLayout
        component={EditType}
        exact
        layout={DefaultLayout}
        path="/types/:id/update"
      />

      <RouteWithLayout
        component={Settings}
        layout={DefaultLayout}
        exact
        path="/settings"
      />
      <Route component={NotFound} exact path="/not-found" />
      <Route component={Login} exact path="/login" />

      <Redirect to="/not-found" />
    </Switch>
  );
};

export default Routes;
